//instatiate XHR object
var xhr = new XMLHttpRequest();

//get json text and put in div
xhr.onreadystatechange = function()
{
	list = JSON.parse(xhr.responseText);
	lst = document.getElementById('list');
	stat = document.getElementById('status');
	
	if (xhr.status == 200 && xhr.readyState == xhr.DONE)
	{
		stat.innerHTML = xhr.status;
		lst.innerHTML = xhr.responseText;
	}
};

//make request
xhr.open('GET','todoList.json');
xhr.send();
